---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: filesbackup
  name: filesbackup
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: filesbackup
      annotations:
        backup.velero.io/backup-volumes: codebase 
    spec:
      # affinity:
      #   podAffinity:
      #     requiredDuringSchedulingIgnoredDuringExecution:
      #     - labelSelector:
      #         matchExpressions:
      #         - key: app
      #           operator: In
      #           values:
      #           - web
      #       topologyKey: "kubernetes.io/hostname"
      containers:
      - image: busybox
        imagePullPolicy: IfNotPresent
        name: filesbackup
        args:
          - sleep
          - "1000000"
        volumeMounts:
        - mountPath: /filesbackup
          name: codebase
          readOnly: false
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: db
  name: db
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
    # indicate which strategy we want for rolling update
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: db
      annotations:
        backup.velero.io/backup-volumes: db
    spec:
      # affinity:
      #   podAffinity:
      #     requiredDuringSchedulingIgnoredDuringExecution:
      #     - labelSelector:
      #         matchExpressions:
      #         - key: app
      #           operator: In
      #           values:
      #           - web
      #       topologyKey: "kubernetes.io/hostname"
      containers:
      - env:
        {{- if .Values.db.extraVars }}
{{ toYaml .Values.db.extraVars | indent 10 }}
        {{- end }}
        image: "{{ .Values.db.image.repository }}:{{ .Values.db.image.tag }}"
        imagePullPolicy: "{{ .Values.db.image.pullPolicy }}"
        name: "{{ .Values.db.name }}"
        resources:
{{ toYaml .Values.db.resources | indent 10 }}
        stdin: true
        tty: true
        volumeMounts:
        - mountPath: /var/lib/mysql
          name: db
          readOnly: false
          subPath: database
        ports:
        - containerPort: 3306
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 3
          initialDelaySeconds: 20
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
      restartPolicy: Always
      volumes:
      - name: db
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk-db
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: backup
  name: backup
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: backup
    spec:
      # affinity:
      #   podAffinity:
      #     requiredDuringSchedulingIgnoredDuringExecution:
      #     - labelSelector:
      #         matchExpressions:
      #         - key: app
      #           operator: In
      #           values:
      #           - web
      #       topologyKey: "kubernetes.io/hostname"
      containers:
      - env:
        {{- if .Values.backup.extraVars }}
{{ toYaml .Values.backup.extraVars | indent 10 }}
        {{- end }}
        image: "{{ .Values.backup.image.repository }}:{{ .Values.backup.image.tag }}"
        imagePullPolicy: "{{ .Values.backup.image.pullPolicy }}"
        name: "{{ .Values.backup.name }}"
        resources:
{{ toYaml .Values.backup.resources | indent 10 }}
        volumeMounts:
        - mountPath: /backup
          name: codebase
          readOnly: false
          subPath: dbbackup
      initContainers:
      - command:
        - chown
        - -R
        - "1000:1000"
        - /backup
        image: busybox
        imagePullPolicy: IfNotPresent
        name: init-permissions
        resources: {}
        volumeMounts:
        - mountPath: /backup
          name: codebase
          readOnly: false
          subPath: dbbackup
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}

{{- if eq .Values.pma.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: pma
  name: pma
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: pma
    spec:
      containers:
      - env:
        {{- if .Values.pma.extraVars }}
{{ toYaml .Values.pma.extraVars | indent 10 }}
        {{- end }}
        image: "{{ .Values.pma.image.repository }}:{{ .Values.pma.image.tag }}"
        imagePullPolicy: "{{ .Values.pma.image.pullPolicy }}"
        name: "{{ .Values.pma.name }}"
        resources:
{{ toYaml .Values.pma.resources | indent 10 }}
      restartPolicy: Always
status: {}
{{- end }}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: redis
  name: redis
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: redis
    spec:
      # affinity:
      #   podAffinity:
      #     requiredDuringSchedulingIgnoredDuringExecution:
      #     - labelSelector:
      #         matchExpressions:
      #         - key: app
      #           operator: In
      #           values:
      #           - web
      #       topologyKey: "kubernetes.io/hostname"
      containers:
      - image: "{{ .Values.redis.image.repository }}:{{ .Values.redis.image.tag }}"
        imagePullPolicy: "{{ .Values.redis.image.pullPolicy }}"
        name: "{{ .Values.redis.name }}"
        ports:
        - containerPort: 6379
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        resources:
{{ toYaml .Values.redis.resources | indent 10 }}
        volumeMounts:
        - mountPath: /host-sys
          name: host-sys
        - mountPath: /data
          name: codebase
          subPath: redis
        stdin: true
        tty: true
      initContainers:
      - command:
        - sysctl
        - -w
        - net.core.somaxconn=65535
        image: busybox
        imagePullPolicy: Always
        name: sysctl
        resources: {}
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      - command:
        - sh
        - -c
        - '[[ -f /host-sys/kernel/mm/transparent_hugepage/enabled ]] && echo never >
          /host-sys/kernel/mm/transparent_hugepage/enabled'
        image: busybox
        imagePullPolicy: Always
        name: disable-thp
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /host-sys
          name: host-sys
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
      - hostPath:
          path: /sys
        name: host-sys
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: php
  name: php
  namespace: {{ .Values.name }}
spec:
  selector:
    matchLabels:
      app: web
  replicas: 1
  strategy:
    #  indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: php
        app: web
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: io.kompose.service
                operator: In
                values:
                - php
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
        - name: DB_HOST
          value: db
        - name: DB_USER
          value: root
        - name: DB_PASSWORD
          value: "admin123"
        - name: DB_NAME
          value: drupal
        - name: DB_DRIVER
          value: mysql
{{- if eq .Values.dev.enabled "yes" }}
        - name: PHP_XHPROF
          value: "1"
        - name: PHP_BLACKFIRE
          value: "0"
        - name: PHP_XDEBUG
          value: "0"
        - name: XDEBUG_PROFILE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER_VALUE
          value: "0"
{{- end}}
        - name: PHP_SITE_NAME
          value: "{{ .Values.name }}"
        - name: HTTP_ROOT
          value: /var/www/html/docroot/web
        - name: DOCROOT_SUBDIR
          value: docroot/web
        - name: SSH_PRIVATE_KEY
          value: '{{ .Values.php.ssh_priv_key | quote }}'
        - name: SSH_PUBLIC_KEY
          value: '{{ .Values.php.ssh_public_key | quote }}'
        - name: WODBY_APP_SUBSITE
          value: default
        - name: HTTP_ROOT
          value: /var/www/html/docroot/web
        - name: APP_ROOT
          value: /var/www/html
        - name: CONF_DIR
          value: /var/www/conf
        - name: DRUPAL_VERSION
          value: "8"
        - name: DRUPAL_SITE
          value: "default"
        - name: DRUPAL_SITE_DIR
          value: "/var/www/html/docroot/web/sites/default"
        - name: DRUPAL_HASH_SALT
          value: "rocket-path-bad-hash-salt"
        - name: DRUPAL_FILES_SYNC_SALT
          value: "rocket-path-files-sync-bad-salt"
        - name: WODBY_HOSTS
          value: |-
            ["{{ .Values.virtualHost }}"{{- if .Values.virtualHost2 }},"{{ .Values.virtualHost2 }}"{{- end }}]
        - name: WODBY_HOST_PRIMARY
          value: "{{ .Values.virtualHost }}"
        - name: WODBY_URL_PRIMARY
          value: "https://{{ .Values.virtualHost }}"
        - name: PHP_FPM_ENV_VARS
          value: '["APP_ROOT","CONF_DIR","FILES_DIR","HTTP_ROOT","WODBY_APP_DOCROOT","WODBY_APP_NAME","WODBY_APP_ROOT","WODBY_APP_UUID","WODBY_CONF","WODBY_DIR_CONF","WODBY_ENVIRONMENT_NAME","WODBY_ENVIRONMENT_TYPE","WODBY_HOSTS","WODBY_HOST_PRIMARY","WODBY_INSTANCE_NAME","WODBY_INSTANCE_TYPE","WODBY_INSTANCE_UUID","WODBY_URL_PRIMARY"]'
        - name: PHP_OPCACHE_ENABLE
          value: "1"
        - name: PHP_ERROR_REPORTING
          value: E_ALL & ~E_DEPRECATED & ~E_STRICT
        - name: PHP_DISPLAY_ERRORS
          value: "Off"
        - name: PHP_DISPLAY_STARTUP_ERRORS
          value: "Off"
        - name: PHP_TRACK_ERRORS
          value: "Off"
{{- if eq .Values.varnish.enabled "yes" }}
        - name: VARNISH_HOST
          value: "varnish"
        - name: VARNISH_TERMINAL_PORT
          value: "6082"
        - name: VARNISH_SECRET
          value: "secret"
        - name: VARNISH_VERSION
          value: "4"
{{- end }}
{{- if eq .Values.redis.enabled "yes" }}
        - name: REDIS_HOST
          value: "redis"
        - name: REDIS_PORT
          value: "6379"
        - name: REDIS_PASSWORD
          value: "redis"
{{- end}}
{{- if eq .Values.opensmtpd.enabled "yes" }}
        - name: PHP_SENDMAIL_PATH
          value: /usr/sbin/sendmail -t -i -S opensmtpd:25
{{- end}}
        - name: WODBY_APP_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_TYPE
          value: prod
        - name: WODBY_ENVIRONMENT_NAME
          value: prod
        - name: WODBY_ENVIRONMENT_TYPE
          value: prod
          # {{ toYaml .Values.php.extraVars | indent 8 }}
        image: "{{ .Values.php.image.repository }}:{{ .Values.php.image.tag }}"
        imagePullPolicy: "{{ .Values.php.image.pullPolicy }}"
        name: "{{ .Values.php.name }}"
        resources:
{{ toYaml .Values.php.resources | indent 10 }}
        ports:
        - containerPort: 9000
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
        volumeMounts:
        - mountPath: /var/www/html
          name: codebase
          readOnly: false
          subPath: codebase
        - mountPath: /mnt/files
          name: codebase
          readOnly: false
          subPath: files
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: nginx
  name: nginx
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: nginx
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: io.kompose.service
                operator: In
                values:
                - nginx
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
        - name: NGINX_VHOST_PRESET
          value: "drupal8"
        - name: NGINX_SERVER_ROOT
          value: "/var/www/html/docroot/web"
        - name: NGINX_DRUPAL_ALLOW_XML_ENDPOINTS
          value: "1"
        - name: NGINX_SET_REAL_IP_FROM
          value: "10.233.64.0/18"
        - name: NGINX_CLIENT_BODY_TIMEOUT
          value: "120"
        - name: NGINX_CLIENT_HEADER_TIMEOUT
          value: "120"
        - name: NGINX_BACKEND_HOST
          value: php
        - name: NGINX_SERVER_NAME
          value: {{ .Values.virtualHost }}
{{- if eq .Values.nginx.modpagespeed.enabled "yes" }}
        - name: NGINX_PAGESPEED_ENABLED
          value: "1"
        - name: NGINX_SERVER_EXTRA_CONF_FILEPATH
          value: "/var/www/html/extra.conf" 
{{- end }}
{{ if .Values.nginx.extraVars -}}
{{ toYaml .Values.nginx.extraVars | indent 8 }}
{{- end }}
        image: "{{ .Values.nginx.image.repository }}:{{ .Values.nginx.image.tag }}"
        imagePullPolicy: "{{ .Values.nginx.image.pullPolicy }}"
        name: "{{ .Values.nginx.name }}"
        resources:
{{ toYaml .Values.nginx.resources | indent 12 }}
        ports:
        - containerPort: 80
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
        volumeMounts:
        - mountPath: /var/www/html
          name: codebase
          readOnly: false
          subPath: codebase
        - mountPath: /mnt/files
          name: codebase
          readOnly: false
          subPath: files
{{- if eq .Values.nginx.modpagespeed.enabled "yes" }}
        - mountPath: /var/www/html/extra.conf
          subPath: extra.conf
          name: extra
          readOnly: false
{{- end }}
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
{{- if eq .Values.nginx.modpagespeed.enabled "yes" }}
      - name: extra
        configMap:
          name: {{ .Values.name }}-modpagespeed
          items:
          - key: extra.conf
            path: extra.conf
{{- end }}
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: crond
  name: crond
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: crond
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - web
            topologyKey: "kubernetes.io/hostname"
      containers:
      - args:
        - sudo
        - -E
        - crond
        - -f
        - -d
        - "0"
        env:
        - name: DB_HOST
          value: db
        - name: DB_USER
          value: root
        - name: DB_PASSWORD
          value: "admin123"
        - name: DB_NAME
          value: drupal
        - name: DB_DRIVER
          value: mysql
{{- if eq .Values.dev.enabled "yes" }}
        - name: PHP_XHPROF
          value: "1"
        - name: PHP_BLACKFIRE
          value: "0"
        - name: PHP_XDEBUG
          value: "0"
        - name: XDEBUG_PROFILE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER_VALUE
          value: "0"
{{- end}}
        - name: PHP_SITE_NAME
          value: "{{ .Values.name }}"
        - name: PHP_MAX_INPUT_VARS
          value: "15000"
        - name: HTTP_ROOT
          value: /var/www/html/docroot/web
        - name: DOCROOT_SUBDIR
          value: docroot/web
        - name: SSH_PRIVATE_KEY
          value: {{ .Values.php.ssh_priv_key | quote }}
        - name: SSH_PUBLIC_KEY
          value: '{{ .Values.php.ssh_public_key | quote }}'
        - name: WODBY_APP_SUBSITE
          value: default
        - name: WODBY_APP_DOCROOT
          value: /var/www/html/docroot/web
        - name: WODBY_APP_ROOT
          value: /var/www/html
        - name: WODBY_DIR_CONF
          value: /var/www/conf
        - name: WODBY_CONF
          value: /var/www/conf
        - name: DRUPAL_VERSION
          value: "8"
        - name: DRUPAL_SITE
          value: "default"
        - name: DRUPAL_SITE_DIR
          value: "/var/www/html/docroot/web/sites/default"
        - name: DRUPAL_HASH_SALT
          value: "rocket-path-bad-hash-salt"
        - name: DRUPAL_FILES_SYNC_SALT
          value: "rocket-path-files-sync-bad-salt"
        - name: WODBY_HOSTS
          value: |-
            ["{{ .Values.virtualHost }}"{{- if .Values.virtualHost2 }},"{{ .Values.virtualHost2 }}"{{- end }}]
        - name: WODBY_HOST_PRIMARY
          value: "{{ .Values.virtualHost }}"
        - name: WODBY_URL_PRIMARY
          value: "https://{{ .Values.virtualHost }}"
        - name: PHP_FPM_ENV_VARS
          value: '["APP_ROOT","CONF_DIR","FILES_DIR","HTTP_ROOT","WODBY_APP_DOCROOT","WODBY_APP_NAME","WODBY_APP_ROOT","WODBY_APP_UUID","WODBY_CONF","WODBY_DIR_CONF","WODBY_ENVIRONMENT_NAME","WODBY_ENVIRONMENT_TYPE","WODBY_HOSTS","WODBY_HOST_PRIMARY","WODBY_INSTANCE_NAME","WODBY_INSTANCE_TYPE","WODBY_INSTANCE_UUID","WODBY_URL_PRIMARY"]'
        - name: PHP_OPCACHE_ENABLE
          value: "1"
        - name: PHP_ERROR_REPORTING
          value: E_ALL & ~E_DEPRECATED & ~E_STRICT
        - name: PHP_DISPLAY_ERRORS
          value: "Off"
        - name: PHP_DISPLAY_STARTUP_ERRORS
          value: "Off"
        - name: PHP_TRACK_ERRORS
          value: "Off"
{{- if eq .Values.varnish.enabled "yes" }}
        - name: VARNISH_HOST
          value: "varnish"
        - name: VARNISH_TERMINAL_PORT
          value: "6082"
        - name: VARNISH_SECRET
          value: "secret"
        - name: VARNISH_VERSION
          value: "4"
{{- end }}
{{- if eq .Values.redis.enabled "yes" }}
        - name: REDIS_HOST
          value: "redis"
        - name: REDIS_PORT
          value: "6379"
        - name: REDIS_PASSWORD
          value: "redis"
{{- end}}
{{- if eq .Values.opensmtpd.enabled "yes" }}
        - name: PHP_SENDMAIL_PATH
          value: /usr/sbin/sendmail -t -i -S opensmtpd:25
{{- end}}
        - name: WODBY_APP_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_TYPE
          value: prod
        - name: WODBY_ENVIRONMENT_NAME
          value: prod
        - name: WODBY_ENVIRONMENT_TYPE
          value: prod
{{- if .Values.crond.extraVars }}
{{ toYaml .Values.crond.extraVars | indent 8 }}
{{- end }}
        image: "{{ .Values.crond.image.repository }}:{{ .Values.crond.image.tag }}"
        imagePullPolicy: "{{ .Values.crond.image.pullPolicy }}"
        name: "{{ .Values.crond.name }}"
        resources:
{{ toYaml .Values.crond.resources | indent 12 }}
        volumeMounts:
        - mountPath: /var/www/html
          name: codebase
          readOnly: false
          subPath: codebase
        - mountPath: /mnt/files
          name: codebase
          readOnly: false
          subPath: files
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}

---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: sshd
  name: sshd
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: sshd
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - web
            topologyKey: "kubernetes.io/hostname"
      containers:
      - args:
        - sudo
        - /usr/sbin/sshd
        - -De
        env:
        - name: DB_HOST
          value: db
        - name: DB_USER
          value: root
        - name: DB_PASSWORD
          value: "admin123"
        - name: DB_NAME
          value: drupal
        - name: DB_DRIVER
          value: mysql
{{- if eq .Values.dev.enabled "yes" }}
        - name: PHP_XHPROF
          value: "1"
        - name: PHP_BLACKFIRE
          value: "0"
        - name: PHP_XDEBUG
          value: "0"
        - name: XDEBUG_PROFILE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER
          value: "0"
        - name: PHP_XDEBUG_PROFILER_ENABLE_TRIGGER_VALUE
          value: "0"
{{- end}}
        - name: PHP_SITE_NAME
          value: "{{ .Values.name }}"
        - name: PHP_MAX_INPUT_VARS
          value: "6000"
        - name: HTTP_ROOT
          value: /var/www/html/docroot/web
        - name: DOCROOT_SUBDIR
          value: docroot/web
        - name: SSH_PRIVATE_KEY
          value: '[{{ .Values.php.ssh_priv_key | quote }}]'
        - name: SSH_PUBLIC_KEYS
          value: '[{{ .Values.php.ssh_public_key | quote }}]'
        - name: WODBY_APP_SUBSITE
          value: default
        - name: WODBY_APP_DOCROOT
          value: /var/www/html/docroot/web
        - name: WODBY_APP_ROOT
          value: /var/www/html
        - name: WODBY_DIR_CONF
          value: /var/www/conf
        - name: WODBY_CONF
          value: /var/www/conf
        - name: DRUPAL_VERSION
          value: "8"
        - name: DRUPAL_SITE
          value: "default"
        - name: DRUPAL_SITE_DIR
          value: "/var/www/html/docroot/web/sites/default"
        - name: DRUPAL_HASH_SALT
          value: "rocket-path-bad-hash-salt"
        - name: DRUPAL_FILES_SYNC_SALT
          value: "rocket-path-files-sync-bad-salt"
        - name: WODBY_HOSTS
          value: |-
            ["{{ .Values.virtualHost }}"{{- if .Values.virtualHost2 }},"{{ .Values.virtualHost2 }}"{{- end }}]
        - name: WODBY_HOST_PRIMARY
          value: "{{ .Values.virtualHost }}"
        - name: WODBY_URL_PRIMARY
          value: "https://{{ .Values.virtualHost }}"
        - name: PHP_FPM_ENV_VARS
          value: '["APP_ROOT","CONF_DIR","FILES_DIR","HTTP_ROOT","WODBY_APP_DOCROOT","WODBY_APP_NAME","WODBY_APP_ROOT","WODBY_APP_UUID","WODBY_CONF","WODBY_DIR_CONF","WODBY_ENVIRONMENT_NAME","WODBY_ENVIRONMENT_TYPE","WODBY_HOSTS","WODBY_HOST_PRIMARY","WODBY_INSTANCE_NAME","WODBY_INSTANCE_TYPE","WODBY_INSTANCE_UUID","WODBY_URL_PRIMARY"]'
        - name: PHP_OPCACHE_ENABLE
          value: "1"
        - name: PHP_ERROR_REPORTING
          value: E_ALL & ~E_DEPRECATED & ~E_STRICT
        - name: PHP_DISPLAY_ERRORS
          value: "Off"
        - name: PHP_DISPLAY_STARTUP_ERRORS
          value: "Off"
        - name: PHP_TRACK_ERRORS
          value: "Off"
{{- if eq .Values.varnish.enabled "yes" }}
        - name: VARNISH_HOST
          value: "varnish"
        - name: VARNISH_TERMINAL_PORT
          value: "6082"
        - name: VARNISH_SECRET
          value: "secret"
        - name: VARNISH_VERSION
          value: "4"
{{- end }}
{{- if eq .Values.redis.enabled "yes" }}
        - name: REDIS_HOST
          value: "redis"
        - name: REDIS_PORT
          value: "6379"
        - name: REDIS_PASSWORD
          value: "redis"
{{- end}}
{{- if eq .Values.opensmtpd.enabled "yes" }}
        - name: PHP_SENDMAIL_PATH
          value: /usr/sbin/sendmail -t -i -S opensmtpd:25
{{- end}}
        - name: WODBY_APP_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_NAME
          value: "{{ .Values.name }}"
        - name: WODBY_INSTANCE_TYPE
          value: prod
        - name: WODBY_ENVIRONMENT_NAME
          value: prod
        - name: WODBY_ENVIRONMENT_TYPE
          value: prod
        - name: SSHD_PASSWORD_AUTHENTICATION
          value: "yes"
        image: "{{ .Values.sshd.image.repository }}:{{ .Values.sshd.image.tag }}"
        imagePullPolicy: "{{ .Values.sshd.image.pullPolicy }}"
        name: "{{ .Values.sshd.name }}"
        resources:
{{ toYaml .Values.sshd.resources | indent 12 }}
        ports:
        - containerPort: 22
          protocol: TCP
        - containerPort: 9000
          protocol: TCP
        volumeMounts:
        - mountPath: /var/www/html
          name: codebase
          readOnly: false
          subPath: codebase
        - mountPath: /mnt/files
          name: codebase
          readOnly: false
          subPath: files
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}

{{- if eq .Values.node.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: node
  name: node
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: node
    spec:
      containers:
      - image: "{{ .Values.node.image.repository }}:{{ .Values.node.image.tag }}"
        imagePullPolicy: "{{ .Values.node.image.pullPolicy }}"
        name: "{{ .Values.node.name }}"
        workingDir: {{ .Values.node.workingDir }}
        command: {{ .Values.node.command }}
        args: {{ .Values.node.args }}
        resources:
{{ toYaml .Values.node.resources | indent 12 }}
        volumeMounts:
        - mountPath: /var/www/html
          name: codebase-node
          readOnly: false
      restartPolicy: Always
      nodeSelector:
        hostname: {{ .Values.hostname }}
      volumes:
      - name: codebase-node
        hostPath:
          path: {{ .Values.diskpath }}
status: {}
{{- end }}

{{- if eq .Values.solr.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: solr
  name: solr
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: solr
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - web
            topologyKey: "kubernetes.io/hostname"
      containers:
      - image: "{{ .Values.solr.image.repository }}:{{ .Values.solr.image.tag }}"
        imagePullPolicy: "{{ .Values.solr.image.pullPolicy }}"
        name: "{{ .Values.solr.name }}"
        env:
          - name: SOLR_DEFAULT_CONFIG_SET
            value: "search_api_solr_7.x-1.12"
        ports:
        - containerPort: 8983
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 3
          initialDelaySeconds: 10
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        resources:
{{ toYaml .Values.solr.resources | indent 12 }}
        volumeMounts:
        - mountPath: /opt/solr/server/solr
          name: codebase
          readOnly: false
          subPath: solr-data
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}
{{- end }}

{{- if eq .Values.elasticsearch.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: elasticsearch
  name: elasticsearch
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: elasticsearch
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - web
            topologyKey: "kubernetes.io/hostname"
      containers:
      - image: "{{ .Values.elasticsearch.image.repository }}:{{ .Values.elasticsearch.image.tag }}"
        imagePullPolicy: "{{ .Values.elasticsearch.image.pullPolicy }}"
        name: "{{ .Values.elasticsearch.name }}"
        resources:
{{ toYaml .Values.elasticsearch.resources | indent 12 }}
        volumeMounts:
        - mountPath: /usr/share/elasticsearch/data
          name: codebase
          readOnly: false
          subPath: es-data
        env:
        {{- if .Values.elasticsearch.extraVars }}
{{ toYaml .Values.elasticsearch.extraVars | indent 10 }}
        {{- end }} 
        securityContext:
          capabilities:
            add:
            - IPC_LOCK
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      restartPolicy: Always
      initContainers:
      - command:
        - sysctl
        - -w
        - vm.max_map_count=262144
        image: busybox
        imagePullPolicy: IfNotPresent
        name: init-sysctl
        resources: {}
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}
{{- end }}


{{- if eq .Values.varnish.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: varnish
  name: varnish
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: varnish
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: io.kompose.service
                operator: In
                values:
                - varnish
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
{{- if eq .Values.nginx.modpagespeed.enabled "yes" }}
        - name: VARNISH_PAGESPEED_SECRET_KEY
          value: "secret"
{{- end }}
{{- if .Values.varnish.extraVars }}
{{ toYaml .Values.varnish.extraVars | indent 8 }}
{{- end }}
        image: "{{ .Values.varnish.image.repository }}:{{ .Values.varnish.image.tag }}"
        imagePullPolicy: "{{ .Values.varnish.image.pullPolicy }}"
        name: "{{ .Values.varnish.name }}"
        resources:
{{ toYaml .Values.varnish.resources | indent 12 }}
        ports:
        - containerPort: 6081
          protocol: TCP
        - containerPort: 6082
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
      restartPolicy: Always
status: {}
{{- end }}

{{- if eq .Values.opensmtpd.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: opensmtpd
  name: opensmtpd
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: opensmtpd
    spec:
      containers:
      - env:
        {{- if .Values.opensmtpd.extraVars }}
{{ toYaml .Values.opensmtpd.extraVars | indent 8 }}
        {{- end }} 
        image: "{{ .Values.opensmtpd.image.repository }}:{{ .Values.opensmtpd.image.tag }}"
        imagePullPolicy: "{{ .Values.opensmtpd.image.pullPolicy }}"
        name: "{{ .Values.opensmtpd.name }}"
        resources:
{{ toYaml .Values.opensmtpd.resources | indent 12 }}
        volumeMounts:
        - mountPath: /var/spool/smtpd
          name: codebase
          readOnly: false
          subPath: smtpd
        ports:
        - containerPort: 25
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
      restartPolicy: Always
      volumes:
      - name: codebase
        persistentVolumeClaim:
          claimName: {{ .Values.name }}-persistent-disk
status: {}
{{- end }}

{{- if eq .Values.kibana.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: kibana
  name: kibana
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: kibana
    spec:
      containers:
      - env:
        - name: WODBY_APP_NAME
          value: kibana
        image: "{{ .Values.kibana.image.repository }}:{{ .Values.kibana.image.tag }}"
        imagePullPolicy: "{{ .Values.kibana.image.pullPolicy }}"
        name: "{{ .Values.kibana.name }}"
        resources:
{{ toYaml .Values.kibana.resources | indent 12 }}
        ports:
        - containerPort: 5601
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
      restartPolicy: Always
status: {}
{{- end }}
