{{- if eq .Values.elasticsearch.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: elasticsearch
  name: elasticsearch
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: Recreate
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: elasticsearch
    spec:
      containers:
      - image: "{{ .Values.elasticsearch.image.repository }}:{{ .Values.elasticsearch.image.tag }}"
        imagePullPolicy: "{{ .Values.elasticsearch.image.pullPolicy }}"
        name: "{{ .Values.elasticsearch.name }}"
        resources:
{{ toYaml .Values.elasticsearch.resources | indent 12 }}
        volumeMounts:
        - mountPath: /usr/share/elasticsearch/data
          name: elasticsearch-data
          readOnly: false
        env:
        {{- if .Values.elasticsearch.extraVars }}
{{ toYaml .Values.elasticsearch.extraVars | indent 10 }}
        {{- end }} 
        securityContext:
          capabilities:
            add:
            - IPC_LOCK
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      restartPolicy: Always
      initContainers:
      - command:
        - sysctl
        - -w
        - vm.max_map_count=262144
        image: busybox
        imagePullPolicy: IfNotPresent
        name: init-sysctl
        resources: {}
        securityContext:
          privileged: true
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      nodeSelector:
        hostname: {{ .Values.hostname }}
      volumes:
      - name: elasticsearch-data
        hostPath:
          path: {{ .Values.diskpath }}/elastic-data/{{ .Values.name }}-elasticsearch-data
status: {}
{{- end }}


{{- if eq .Values.kibana.enabled "yes" }}
---

apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: kibana
  name: kibana
  namespace: {{ .Values.name }}
spec:
  replicas: 1
  strategy:
  # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
  template:
    metadata:
      creationTimestamp: null
      labels:
        io.kompose.service: kibana
    spec:
      containers:
      - env:
        - name: WODBY_APP_NAME
          value: kibana
        image: "{{ .Values.kibana.image.repository }}:{{ .Values.kibana.image.tag }}"
        imagePullPolicy: "{{ .Values.kibana.image.pullPolicy }}"
        name: "{{ .Values.kibana.name }}"
        resources:
{{ toYaml .Values.kibana.resources | indent 12 }}
        ports:
        - containerPort: 5601
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - make
            - check-ready
            - -f
            - /usr/local/bin/actions.mk
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 3
        stdin: true
        tty: true
      restartPolicy: Always
      nodeSelector:
        hostname: {{ .Values.hostname }}
status: {}
{{- end }}
