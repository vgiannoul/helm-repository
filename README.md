# Custom HELM repository

[![pipeline status](https://gitlab.com/vgiannoul/helm-repository/badges/master/pipeline.svg)](https://gitlab.com/vgiannoul/helm-repository/commits/master)
[![coverage report](https://gitlab.com/vgiannoul/helm-repository/badges/master/coverage.svg)](https://gitlab.com/vgiannoul/helm-repository/commits/master)

To use your new Chart repository, run 
$ helm repo add squared https://vgiannoul.gitlab.io/helm-repository on your local computer.

## Curently available charts 

- drupal 6
- drupal 7
- drupal 8
- wordpress

use like this
helm install --name demowebsite squared/drupal7